
// Mock Databe
let posts = [];

// Post ID
let count = 1;

// Reactive DOM with JSON (CRUD Operation)

// ADD Post Data

document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	// Prevents the default behavior of an event. To prevent the page from reloading (default behavior of submit)

	e.preventDefault();

	posts.push({

		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});
	count++;

	console.log(posts);
	alert("Posts Successfully Added!");
	showPosts();
});

// RETRIEVE Post Data

const showPosts = () => {

	// variable that will contain all the posts
	let postEntries = "";

	// Looping through array items
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>`
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// EDIT Post (Edit Button)

const editPost = (id) => {

	// The function first uses the querySelector() method t oget the element with the id "#post-title-${id}" and "post-body-${id}" and assign its innerHTML property to the title variable with the same body.

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

// UPDATE Post (Update Button)

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++)
	{
		if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value)
		{
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPosts(posts);
			alert("Post Successfully Updated!")
		}
	}
});

// DELETE Post (Delete Button) 

const deletePost = (ids) => {

	posts.filter((data) =>
	{
		let title = document.querySelector(`#post-title-${data.id}`).innerHTML;
		let body = document.querySelector(`#post-body-${data.id}`).innerHTML;

		posts.splice(data.id,1,);
		
	});
	showPosts();
		alert("Successfully Deleted!")
};